#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <emmintrin.h>
#include <nmmintrin.h>
#define RAND_SQR 4611686014132420610 // (2^31-1)^2+1

typedef struct payload{
	long long runs;
	long long pi_estimate;
	pthread_mutex_t lock;
}payload;

void* estimate_pi( void* arg ){
	payload *input = ( payload* ) arg;
	long long number_of_tosses = (*input).runs;
	long long number_in_circle = 0;
	unsigned int seed = ( unsigned int )pthread_self() ^ time(NULL);
	long long toss = 0;

	__m128i stora0 = _mm_set_epi64x(	2801247362,	 372922539 );
	__m128i stora1 = _mm_set_epi64x(	 123456789,	2939983983 );
	__m128i stora2 = _mm_set_epi64x( rand_r(&seed),	 362436069 );
	__m128i stora3 = _mm_set_epi64x(	3839279122,	1487178804 );

	__m128i storb0 = _mm_set_epi64x(	4221117106,	2326480760 );
	__m128i storb1 = _mm_set_epi64x(	 521288629, rand_r(&seed) );
	__m128i storb2 = _mm_set_epi64x(	1819138869,	  88675123 );
	__m128i storb3 = _mm_set_epi64x(	3510435306,	1573257487 );

	__m128i one	= _mm_set_epi64x(		 1,		 1 );
	__m128i output = _mm_set_epi64x(		 0,		 0 );
	__m128i max	= _mm_set_epi64x(  RAND_SQR,  RAND_SQR );
	__m128i mask   = _mm_set_epi64x(2147483647,2147483647 );

	for( toss = 0; toss < number_of_tosses; ++toss ){

		__m128i ta	 = stora3;
		__m128i tb	 = storb3;
		__m128i sa,sb;

		// t ^= t << 11;
		ta = _mm_xor_si128( _mm_slli_epi64( ta, 11 ), ta );
		tb = _mm_xor_si128( _mm_slli_epi64( tb, 11 ), tb );

		// t ^= t >> 8;
		ta = _mm_xor_si128( _mm_srli_epi64( ta, 8 ), ta );
		tb = _mm_xor_si128( _mm_srli_epi64( tb, 8 ), tb );

		// state[3] = state[2]; state[2] = state[1]; state[1] = s = state[0];
		stora3 = stora2; stora2 = stora1; stora1 = sa = stora0;
		storb3 = storb2; storb2 = storb1; storb1 = sb = storb0;

		// t ^= s;
		ta = _mm_xor_si128( ta, sa );
		tb = _mm_xor_si128( tb, sb );

		// t ^= s >> 19;
		ta = _mm_xor_si128( _mm_srli_epi64( sa, 19 ), ta );
		tb = _mm_xor_si128( _mm_srli_epi64( sb, 19 ), tb );

		// state[0] = t;
		stora0 = ta;
		storb0 = tb;

		// return t;

		// case to RAND_MAX
		ta = _mm_and_si128  ( ta, mask );
		tb = _mm_and_si128  ( tb, mask );

		// Square and sum
		ta = _mm_mul_epu32  ( ta,   ta );
		tb = _mm_mul_epu32  ( tb,   tb );
		ta = _mm_add_epi64  ( ta,   tb );

		//check if square less than 1 ( without divide )
		sa = _mm_cmpgt_epi64( max,   ta );

		// convert result to one for adding
		sa = _mm_and_si128  ( sa,  one );

		// accumulate
		output = _mm_add_epi64( output, sa );
	}
	pthread_mutex_lock( &( (*input).lock ) );
	(*input).pi_estimate += _mm_extract_epi64( output, 0 ) + _mm_extract_epi64( output, 1 );
	pthread_mutex_unlock( &( (*input).lock ) );
}

int main(int argc, char *argv[]){
	if( argc != 3 ){
		fprintf(stderr, "Two arguments needed\n");
		exit(EXIT_FAILURE);
	}
	else{
		payload start;
		start.pi_estimate = 0;
		int threads = atoi( argv[argc - 2 ] );
		long long runs = atoll( argv[argc - 1 ] ) / threads;
		start.runs = ( runs >> 1 ) + ( runs & 1 );
		//printf("Running %lld RUNs on %d threads\n", start.runs, threads);
		pthread_t thread[ threads ];
		pthread_mutex_init( &start.lock, NULL );
		pthread_mutex_lock( &( start.lock ) );

		int i;
		for( i = 0; i < threads ; ++i ){
			if( pthread_create( &thread[i], NULL, estimate_pi, ( void * )&start ) ){
				fprintf(stderr, "Error creating thread\n");
				exit(EXIT_FAILURE);
			}
		}

		pthread_mutex_unlock( &( start.lock ) );

		for( i = 0; i < threads ; ++i ){
			if( pthread_join( thread[i], NULL ) ){
				fprintf(stderr, "Error join thread\n");
				exit(EXIT_FAILURE);
			}
		}

		printf( "Estimates PI = %lf\n", 2 * start.pi_estimate / (double)( start.runs * threads ) ); // 4 * start.pi_estimate / (double)( 2 * start.runs * threads ) )
	}
}
